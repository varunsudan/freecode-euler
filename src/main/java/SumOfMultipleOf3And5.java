import org.junit.Assert;
import org.junit.Test;

public class SumOfMultipleOf3And5 {

    public long sol(int number) {
        long sum = 0l;
        for(int i = 3; i < number; i++) {
            if(i%3 == 0) sum += i;
            else if(i%5 == 0) sum += i;
        }

        return sum;
    }

    @Test
    public void testSol() {
        Assert.assertEquals(23l, sol(10));
        Assert.assertEquals(543l, sol(49));
        Assert.assertEquals(233168l, sol(1000));
        Assert.assertEquals(16687353l, sol(8456));
        Assert.assertEquals(89301183l, sol(19564));
    }
}
