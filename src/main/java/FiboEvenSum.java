import org.junit.Assert;
import org.junit.Test;

public class FiboEvenSum {

    public long sol(int n) {
        if(n <= 1) return 0l;
        if(n <= 3) return 2l;

        long sum = 2l;

        int first = 1;
        int second = 2;
        int temp = 0;

        while(second <= n) {
            temp = second + first;
            if(temp <= n && (temp%2 == 0)) {
                sum += temp;
            }
            first = second;
            second = temp;
        }

        return sum;
    }


    @Test
    public void testSol() {
        Assert.assertEquals(0l, sol(-1));
        Assert.assertEquals(0l, sol(1));
        Assert.assertEquals(2l, sol(3));
        Assert.assertEquals(2l, sol(2));
        Assert.assertEquals(10l, sol(10));
        Assert.assertEquals(44l, sol(34));
        Assert.assertEquals(798l, sol(1000));
        Assert.assertEquals(60696l, sol(100000));
        Assert.assertEquals(4613732l, sol(4000000));

    }
}
